data "aws_route53_zone" "default" {
  provider     = "aws.route53"
  name         = "${var.hosted_zone}."
  private_zone = false
}

resource "aws_acm_certificate" "default" {
  domain_name               = "${var.domain_name}"
  subject_alternative_names = ["${var.subject_alternative_names}"]
  validation_method         = "${var.validation_method}"
  tags                      = "${var.tags}"
}

locals {
  domain_validation_options = "${aws_acm_certificate.default.domain_validation_options[0]}"
}

#
# Creating Route 53 DNS record to auto-validate ACM certificate request (only if validation_method == 'DNS')
#
resource "aws_route53_record" "default" {
  count    = "${var.proces_domain_validation_options == "true" && var.validation_method == "DNS" ? 1 : 0}"
  provider = "aws.route53"
  zone_id  = "${data.aws_route53_zone.default.zone_id}"
  name     = "${local.domain_validation_options["resource_record_name"]}"
  type     = "${local.domain_validation_options["resource_record_type"]}"
  ttl      = "${var.ttl}"
  records  = ["${local.domain_validation_options["resource_record_value"]}"]
}

#
# Wait for ACM certificate request to be validated
#
resource "aws_acm_certificate_validation" "default" {
  depends_on              = ["aws_acm_certificate.default"]
  certificate_arn         = "${aws_acm_certificate.default.arn}"
  validation_record_fqdns = ["${aws_route53_record.default.*.fqdn }"]
}
