data "aws_region" "current" {}

provider "aws" {
  alias   = "route53"
  profile = "${var.aws_route53_profile}"
  region  = "${data.aws_region.current.name}"
}
