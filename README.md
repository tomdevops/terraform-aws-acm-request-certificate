# terraform-aws-acm-request-certificate

Terraform module to request an ACM certificate for a domain and add a CNAME record to the DNS zone to complete certificate validation.

## Usage

This example will request an SSL certificate for `example.com` domain:

```
module "acm_request_certificate" {
  source                           = "git::https://bitbucket.org/tomdevops/terraform-aws-acm-request-certificate.git?ref=master"
  domain_name                      = "example.com"
  proces_domain_validation_options = "true"
  ttl                              = "300"
}
```

This example will request an SSL certificate for `example.com` domain and all its subdomains `*.example.com`:

```
module "acm_request_certificate" {
  source                           = "git::https://bitbucket.org/tomdevops/terraform-aws-acm-request-certificate.git?ref=master"
  domain_name                      = "example.com"
  proces_domain_validation_options = "true"
  ttl                              = "300"
  subject_alternative_names        = ["*.example.com"]
}
```

## Variables

| Name  | Default  | Description  | Required  |   |
|---|---|---|---|---|
| `hosted_zone`  | - | Route53 Zone handling the domains on the certificate | Yes |   |
| `domain_name`  | -  | A domain name for which the certificate should be issued  |   |   |
| `subject_alternative_names`  | `[]`  | A list of domains that should be SANs in the issued certificate | No |   |
| `validation_method`  | `DNS` | Which method to use for validation, DNS or EMAIL | No |   |
| `proces_domain_validation_options`  | `true` | Flag to enable/disable processing of the record to add to the DNS zone to complete certificate validation | No |   |
| `ttl`  | `300` | The TTL of the record to add to the DNS zone to complete certificate validation | No |   |
| `tags`  | `{}` | Additional tags (e.g. map('BusinessUnit`,`XYZ`) | No |   |
| `aws_route53_profile`  | - | The AWS profile that should be used to modify Route 53 records | Yes |   |

## Outputs

| Name  | Description  |
|---|---|
| `id`  | The ARN of the certificate  |
| `arn`  | The ARN of the certificate  |
| `domain_validation_options`  | CNAME records that are added to the DNS zone to complete certificate validation  |

~ the end